package imagebrowser.control;

import imagebrowser.ui.ImageViewer;

public class NextImageCommand extends BrowseCommand {

    public NextImageCommand(ImageViewer viewer) {
        super(viewer);
    }
    
    @Override
    public void execute() {
        this.getViewer().getNext();
    }

}
